import "./App.css"
import NavBar from './components/Navbar';
import Form from "./components/Form";
import './style/Style.css';
import './style/style.example.css';
import Footer from "./components/Footer";
import Mercedes from "./assets/img/Mercedes.png"

function Cars() {
    return (
        <div>
            <div>
                <NavBar />
            </div>
            <div>
            <div className="container-fluid  mt-5" id="MainSection" style={{backgroundColor:"#8AA899"}}>
        <div className="container mt-5">
            <div className="row main-row">
                <div className="col-lg-6 col-md-6 justify-content-center">
                    <h2 className="mainTitle ">Sewa & Rental Mobil Terbaik di kawasan (Lokasimu)</h2>
                    <p className="mainText">Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas terbaik dengan harga terjangkau. Selalu siap melayani kebutuhanmu untuk sewa mobil selama 24 jam.</p>
                </div>
                <div className="col-lg-6 col-md-6">
                    <img src={Mercedes} alt="" className="img-fluid mobil"/>
                </div>
            </div>
        </div>
    </div>
            </div>
            <div>
                <Form />
            </div>
            <div>
                <Footer />
            </div>
        </div>
    )
}

export default Cars;