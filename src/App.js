import './App.css';
import Body from './components/Body';
import Footer from './components/Footer';
import MainSection from './components/MainSection';
import Navbar from './components/Navbar';
import './style/Style.css';
import './style/style.example.css';

function App() {
  return (
    <div>
      <div>
        <Navbar />
      </div>
      <div>
        <MainSection />
      </div>
      <div>
        <Body />
      </div>
      <div>
        <Footer />
      </div>
    </div>
  );
}

export default App;