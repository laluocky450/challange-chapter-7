import { configureStore } from '@reduxjs/toolkit';
import carsReducer from '../features/counter/carsSlice';

export default configureStore({
  reducer: {
    cars: carsReducer
  }
});