import { useEffect, useState } from "react";
import { BsPeopleFill, BsFillGearFill, BsFillCalendarFill } from "react-icons/bs";
import { Card } from "react-bootstrap";
import "../style/Style.css"
import "../features/counter/Counter"

const Form = () => {

    const [cars, setCars] = useState([])
    const [carsFilter, setCarsFilter] = useState([])
    const [capacity, setCapacity] = useState()
    const [tanggal, setTanggal] = useState()
    const [waktu, setWaktu] = useState()

    useEffect(() => {
        fetch('https://raw.githubusercontent.com/fnurhidayat/probable-garbanzo/main/data/cars.min.json')
        .then((Response) => Response.json())
        .then(data =>{
            setCars(data)
            setCarsFilter(data)
        })
        .catch((err) => console.log(err))

    }, []);

    const filterCars = () => {
        const filteredCars = cars.filter(item => {
            let dateTime = new Date(tanggal + " " + waktu)
            let date = new Date(item.availableAt);
            let newDate = date.getTime()
            const beforeEpochTime = dateTime.getTime()
            // let filterDate = ;
            console.log(beforeEpochTime);
            console.log("newDate", newDate);
            // console.log("filterDate", filterDate);
            return item.capacity <= capacity &&  newDate > beforeEpochTime ;
        })
        console.log("capacity",capacity);
        // console.log(tanggal);
        // console.log(waktu);
        // console.log(filteredCars);
        setCarsFilter(filteredCars)
    }

    return (
        <>
        <div className="form-binar p-3 container">
        <div className="driver">
            <div className="row">
                <label for="cars">Tipe Driver</label>
            </div>
            <div className="col-lg-12 col-md-12">
                <select id="driver" className="form-select" aria-label="Default select example">
                    <option hidden>Tipe Driver</option>
                    <option value="1">Dengan Supir</option>
                    <option value="2">Tanpa Supir (Lepas Kunci)</option>
                </select>
            </div>
        </div>
        <div className="date">
            <div className="row">
                <label for="tanggal">Tanggal</label>
                <div className="col-lg-12 col-md-12">
                    <input placeholder="Pilih Tanggal" type="date" id="tanggal" className="form-control" onChange={(e) => setTanggal((e.target.value))}/>
                </div>
            </div>
        </div>
        <div className="time">
            <div className="row">
                <label for="waktu">Waktu Jemput/Ambil</label>
                <div className="col-lg-12 col-md-12">
                {/* <input type="time" id="waktu" className="form-control" placeholder="00:00" onChange={(e) => setWaktu((e.target.value))}/> */}
                    <select id="waktu" className="form-select" aria-label="Default select example" onInput={(e) => setWaktu((e.target.value))}>
                        <option hidden>Waktu Ambil</option>
                        <option value="08:00">08:00 WIB</option>
                        <option value="09:00">09:00 WIB</option>
                        <option value="10:00">10:00 WIB</option>
                        <option value="11:00">11:00 WIB</option>
                        <option value="12:00">12:00 WIB</option>
                        <option value="13:00">13:00 WIB</option>
                        <option value="14:00">14:00 WIB</option>
                        <option value="15:00">15:00 WIB</option>
                        <option value="16:00">16:00 WIB</option>
                        <option value="17:00">17:00 WIB</option>
                        <option value="18:00">18:00 WIB</option>
                        <option value="19:00">19:00 WIB</option>
                        <option value="20:00">20:00 WIB</option>
                        <option value="21:00">21:00 WIB</option>
                        <option value="22:00">22:00 WIB</option>
                        <option value="23:00">23:00 WIB</option>
                    </select>
                </div>
            </div>
        </div>
        <div className="penumpang">
            <div className="row">
                <label for="penumpang">Jumlah Penumpang (Optional)</label>
                <div className="col-lg-8 col-md-12">
                    <input id="penumpang" size="25"  style={{height:"38px"}} type="text"  placeholder="Masukkan Jumlah Penumpang" onChange={(e) => setCapacity(Number(e.target.value))} />
                </div>
                <div className="col-lg-4">
                    <button className="btn btn-success" id="load-btn" onClick={filterCars}>Cari Mobil</button>
                </div>
            </div>
        </div>
    </div>

    <div className="container" style={{padding: "0 1% 0 1%"}}>
        <div className="row">
            {carsFilter.map(
                item => {
                    return (
                        <div  key={item.id} className="col-md-4 pt-3">
                            <Card className="mb-4 card-filter" style={{border :"1px solid white"}}>
                                <Card.Body>
                                    <img src={item.image} alt={item.manufacture}  className="card-img-top" style={{height:"400px", objectFit:"cover"}} />
                                    <h6 className="mt-2">{item.manufacture} / {item.model}</h6>
                                    <h5><b>Rp.{item.rentPerDay} / Hari </b></h5>
                                    <p className="card-text" style={{fontSize:"14.7px", height:"59px"}}>{item.description}</p>
                                    <p style={{fontSize:"14.7px", height:"29px"}}><BsPeopleFill/> {item.capacity} Orang</p>
                                    <p style={{fontSize:"14.7px", height:"29px"}}><BsFillGearFill/> {item.transmission}</p>
                                    <p style={{fontSize:"14.7px", height:"29px"}}><BsFillCalendarFill/> Tahun {item.year}</p>
                                    <p style={{fontSize:"14.7px", height:"29px"}}>Tersedia:<b>{item.availableAt}</b></p>
                                </Card.Body>
                                <input className="btn btn-success btn-sm mx-3 my-2" type="submit" value="Pilih Mobil"/>
                            </Card>
                        </div>
                    )
                }
            )}
        </div>
    </div>
    </>
    )
}
export default Form;