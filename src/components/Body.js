import ServiceImage from "../assets/img/img_service.png"
import GroupImage from "../assets/img/Group53.png"
import completeIcon from "../assets/img/icon_complete.png"
import priceIcon from "../assets/img/icon_price.png"
import hoursIcon from "../assets/img/icon_24hrs.png"
import professionalIcon from "../assets/img/icon_professional.png"
import newImage from "../assets/img/new1.png"
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';  
import 'owl.carousel/dist/assets/owl.theme.default.css';
// import '../style/Style.css';
// import '../style/style.example.css';
import { Accordion, Col, Row } from "react-bootstrap"
import { BsFillStarFill } from "react-icons/bs"

const Body = () => {
    return (
        <>
        <div className="container our-service" id="OurService">
        <div className="row our-row">
            <div className="col-lg-6 service-img">
                <img src={ServiceImage} className="img-fluid" alt="" />
            </div>
            <div className="col-lg-6 text-service">
                <h3 className="our-title">Best Car Rental for any kind of trip in (Lokasimu)!</h3>
                <p className="our-text pt-3">Sewa mobil di (Lokasimu) bersama Binar Car Rental jaminan harga lebih murah dibandingkan yang lain, kondisi mobil baru, serta kualitas pelayanan terbaik untuk perjalanan wisata, bisnis, wedding, meeting, dll.</p>
                <p><img src={GroupImage} alt="" /><span className="ms-3">Sewa Mobil Dengan Supir di Bali 12 Jam</span></p>
                <p><img src={GroupImage} alt="" /><span className="ms-3">Sewa Mobil Lepas Kunci di Bali 24 Jam</span></p>
                <p><img src={GroupImage} alt="" /><span className="ms-3">Sewa Mobil Jangka Panjang Bulanan</span></p>
                <p><img src={GroupImage} alt="" /><span className="ms-3">Gratis Antar - Jemput Mobil di Bandara</span></p>
                <p><img src={GroupImage} alt="" /><span className="ms-3">Layanan Airport Transfer / Drop In Out</span></p>
            </div>
        </div>
    </div>

    <div className="container why-us" id="WhyUs">
        <div className="main whymain">
            <h1 className="WhyTitle "><strong>Why Us?</strong></h1>
            <p className="WhyText pt-3 pb-3">Mengapa harus pilih Binar Car Rental</p>
        </div>
        <div className="row why-us">
            <div className="col-lg-3 col-md-3">
                <div className="card why-card">
                    <div className="card-body">
                        <img src={completeIcon} alt="" className="pb-3"/>
                        <h6 className="card-title">Mobil Lengkap</h6>
                        <p className="card-text">Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan terawat</p>
                    </div>
                </div>
            </div>
            <div className="col-lg-3 col-md-3">
                <div className="card why-card">
                    <div className="card-body">
                        <img src={priceIcon} alt="" className="pb-3"/>
                        <h6 className="card-title">Harga Murah</h6>
                        <p className="card-text">Harga murah dan bersaing, bisa bandingkan harga kami dengan rental mobil lain</p>
                    </div>
                </div>
            </div>
            <div className="col-lg-3 col-md-3">
                <div className="card why-card">
                    <div className="card-body">
                        <img src={hoursIcon} alt="" className="pb-3"/>
                        <h6 className="card-title">Layanan 24 Jam</h6>
                        <p className="card-text">Siap melayani kebutuhan Anda selama 24 jam nonstop. Kami juga tersedia di akhir minggu</p>
                    </div>
                </div>
            </div>
            <div className="col-lg-3 col-md-3">
                <div className="card why-card">
                    <div className="card-body">
                        <img src={professionalIcon} alt="" className="pb-3"/>
                        <h6 className="card-title">Sopir Profesional</h6>
                        <p className="card-text">Sopir yang profesional, berpengalaman, jujur, ramah dan selalu tepat waktu</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section className="testimonial" id="testimonial">
            <div className="Testimonial-title text-center" id="Testimonial">
                <h2 className=" fw-bold">Testimonial</h2>
                <p>Berbagai review positif dari para pelanggan kami</p>
            </div>

            <OwlCarousel
            items={2}
            className="owl-theme"
            loop
            nav
            center
            margin={10}
            >
            <Row>
                <Col>
                <div className="item">
                    <div className="testi-body d-flex">
                    <div className="card-left img-card">
                        <img src={newImage} alt="" className="rounded-circle"/>
                    </div>
                    <div className="card-right">
                        <div className="faq-icon" style={{color: "gold",}}>
                        <BsFillStarFill />
                        <BsFillStarFill />
                        <BsFillStarFill />
                        <BsFillStarFill />
                        <BsFillStarFill />
                        </div>
                        <p>
                        “Lorem ipsum dolor sit amet, consectetur furete elit, sed
                        do eiusmod lorem ipsum dolor sit amet, consectetur
                        adipiscing elit, sed do eiusmod lorem ipsum dolor sit
                        amet, consectetur adipiscing elit, sed do eiusmod”
                        </p>
                        <p class="fw-bold">John Dee 32, Bromo</p>
                    </div>
                    </div>
                </div>
                </Col>
            </Row>
            <Row>
                <Col>
                <div className="item">
                    <div className="testi-body d-flex">
                    <div className="card-left img-card">
                        <img src={newImage} alt="" className="rounded-circle" />
                    </div>
                    <div className="card-right">
                        <div className="faq-icon" style={{color: "gold",}}>
                        <BsFillStarFill />
                        <BsFillStarFill />
                        <BsFillStarFill />
                        <BsFillStarFill />
                        <BsFillStarFill />
                        </div>
                        <p>
                        “Lorem ipsum dolor sit amet, consectetur furete elit, sed
                        do eiusmod lorem ipsum dolor sit amet, consectetur
                        adipiscing elit, sed do eiusmod lorem ipsum dolor sit
                        amet, consectetur adipiscing elit, sed do eiusmod”
                        </p>
                        <p class="fw-bold">John Dee 32, Bromo</p>
                    </div>
                    </div>
                </div>
                </Col>
            </Row>
            <Row>
                <Col>
                <div className="item">
                    <div className="testi-body d-flex">  
                    <div className="card-left img-card">
                        <img src={newImage} alt="" className="rounded-circle"/>
                    </div>
                    <div className="card-right">
                        <div className="faq-icon" style={{color: "gold",}}>
                        <BsFillStarFill />
                        <BsFillStarFill />
                        <BsFillStarFill />
                        <BsFillStarFill />
                        <BsFillStarFill />
                        </div>
                        <p>
                        “Lorem ipsum dolor sit amet, consectetur furete elit, sed
                        do eiusmod lorem ipsum dolor sit amet, consectetur
                        adipiscing elit, sed do eiusmod lorem ipsum dolor sit
                        amet, consectetur adipiscing elit, sed do eiusmod”
                        </p>
                        <p class="fw-bold">John Dee 32, Bromo</p>
                    </div>
                    </div>
                </div>
                </Col>
            </Row>
            </OwlCarousel>
        </section>

    <div className="container banner">
        <div className="row align-items-center text-center banner-row">
            <div className="col-sm-12 banner-col ">
                <h3 className="title-banner mb-3"><strong>Sewa Mobil di (Lokasimu) Sekarang</strong></h3>
            </div>
            <div className="col-lg-6 col-md-6 offset-sm-3">
                <p className=" text-banner">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
            </div>
            <div className="col-lg-12 col-md-12 mt-3 button-banner">
                <a href="/cars" className="btn btn-primary">Mulai Sewa Mobil</a>
            </div>
            </div>
    </div>

    <div className="container faq" id="FAQ" >
        <div className="row faq-main">
            <div className="col-md-5">
                <h2 className="faq-title pb-2">Frequently Asked Question</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
            </div>
        <div className="col-md-7 faq-acor">
            <Accordion>
                <Accordion.Item eventKey="0">
                    <Accordion.Header>Apa saja syarat yang dibutuhkan?</Accordion.Header>
                    <Accordion.Body>
                    Lorem ipsum dolor sit, amet consectetur adipisicing elit. Beatae officiis deserunt minima temporibus nihil laborum dignissimos perspiciatis, voluptatum ipsum reiciendis pariatur cumque aspernatur eveniet dolore nemo sapiente quae tempore ullam.
                    </Accordion.Body>
                </Accordion.Item>
                <Accordion.Item eventKey="1">
                    <Accordion.Header>Berapa hari minimal sewa mobil lepas kunci?</Accordion.Header>
                    <Accordion.Body>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Quod quia veritatis consequatur necessitatibus quis possimus blanditiis accusamus, ab culpa sint laudantium numquam, distinctio facere et commodi ea pariatur. Expedita, non.
                    </Accordion.Body>
                </Accordion.Item>
                <Accordion.Item eventKey="2">
                    <Accordion.Header>Berapa hari sebelumnya sebaiknya booking sewa mobil?</Accordion.Header>
                    <Accordion.Body>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim, officia tempore totam quisquam vero assumenda ut velit suscipit ab, at id consectetur omnis similique numquam doloribus voluptates voluptatum incidunt facere?
                    </Accordion.Body>
                </Accordion.Item>
                <Accordion.Item eventKey="3">
                    <Accordion.Header>Apakah ada biaya antar-jemput?</Accordion.Header>
                    <Accordion.Body>
                    Lorem, ipsum dolor sit amet consectetur adipisicing elit. Voluptates facere dolor quia minus eius consequuntur exercitationem similique at illum? Quae, totam. Iure totam doloribus, numquam ea error repellendus expedita impedit!
                    </Accordion.Body>
                </Accordion.Item>
                <Accordion.Item eventKey="4">
                    <Accordion.Header> Bagaimana jika terjadi kecelakaan?</Accordion.Header>
                    <Accordion.Body>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Error suscipit magnam laborum quod numquam optio odit esse facere eaque temporibus inventore quos eveniet voluptate similique voluptas id illo, modi blanditiis.
                    </Accordion.Body>
                </Accordion.Item>
            </Accordion>
        </div>
    </div>
    </div>


        </>
    )
}

export default Body;