import { Button, Nav, Navbar } from "react-bootstrap";

const NavBar = () => {
    return (
        <Nav className="navbar navbar-expand-lg navbar-light fixed-top" style={{backgroundColor: "#8AA899"}}>
        <div className="container" style={{backgroundColor: "#8AA899"}}>
            <Navbar.Brand href="#MainSection">Binar Rent Car</Navbar.Brand>
            
            <Button className="navbar-toggler" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasRight" aria-controls="offcanvasRight">
                <span className="navbar-toggler-icon"></span>
            </Button>

            <div className="offcanvas offcanvas-end" tabindex="-1" id="offcanvasRight" aria-labelledby="offcanvasRightLabel">
                <div className="offcanvas-header">
                <h5 id="offcanvasRightLabel">BCR</h5>
                <Button type="button" className="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></Button>
                </div>
            
            <div className="offcanvas-body">
                <ul className="navbar-nav ms-auto navigation">
                    <li className="nav-item px-2">
                        <Nav.Link aria-current="page" href="#OurService">Our Service</Nav.Link>
                    </li>
                    <li className="nav-item px-2">
                        <Nav.Link href="#WhyUs">Why Us</Nav.Link>
                    </li>
                    <li className="nav-item px-2">
                        <Nav.Link href="#Testimonial">Testimonial</Nav.Link>
                    </li>
                    <li className="nav-item px-2">
                        <Nav.Link href="#FAQ">FAQ</Nav.Link>
                    </li>
                    <li className="nav-item px-2">
                        <Nav.Link className="register btn btn-outline-info">Register</Nav.Link>
                    </li>
                </ul>
                </div>
            </div>
            </div>
        </Nav>
    )
}

export default NavBar;