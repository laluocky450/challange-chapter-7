import facebookIcon from "../assets/img/icon_facebook.png"
import instagramIcon from "../assets/img/icon_instagram.png"
import twitchIcon from "../assets/img/icon_twitch.png"
import mailIcon from "../assets/img/icon_mail.png"

const Footer = () => {
    return (
        <footer>
        <div className="container">
            <div className="row foot-main">
                <div className="col-lg-3 col-md-6">
                    <p>Jalan Suroyo No. 161 Mayangan Kota Probolonggo 672000</p>
                    <p>binarcarrental@gmail.com</p>
                    <p>081-233-334-808</p>
                </div>
                <div className="col-lg-2 col-md-6">
                    <div className="navfoot">
                        <p><strong>Our Service</strong></p>
                        <p><strong>Why Us</strong></p>
                        <p><strong>Testimonial</strong></p>
                        <p><strong>FAQ</strong></p>
                    </div>
                </div>
                <div className="col-lg-4 col-md-6 foot-icon">
                    <p className="text-footer">Connect With Us</p>
                    <img src={facebookIcon} alt="" />
                    <img src={instagramIcon} alt="" />
                    <img src={twitchIcon} alt="" />
                    <img src={mailIcon} alt="" />
                    <img src={twitchIcon} alt="" />
                </div>
                <div className="col-lg-3 col-md-6">
                    <p className=" text-foot">© 2022 <b>Binar Rent Car</b> All Right Reserved</p>
                </div>
            </div>
        </div>
    </footer>
    )
}

export default Footer;